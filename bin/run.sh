#!/bin/bash

# Exit on error
set -e

# Get the project directory 
DIR=$(readlink -f "$(dirname $(readlink -f "$0"))"/..)

# Check for Java Home env variable
if [[ -n "$JAVA_HOME" ]]; then
  JAVA_BIN=$JAVA_HOME/bin/java
else
  JAVA_BIN=java
fi

$JAVA_BIN -jar target/filediscovery.jar "$@"